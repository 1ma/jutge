with Ada.Integer_Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Suma de tres enters

procedure p41221 is
   i, j, k: Integer;
begin
   Ada.Integer_Text_IO.Get(i);
   Ada.Integer_Text_IO.Get(j);
   Ada.Integer_Text_IO.Get(k);

   Ada.Integer_Text_IO.Put(Item => i + j + k, Width => 0);
end p41221;
