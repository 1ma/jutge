with Ada.Integer_Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Divisió entera i residu de dos naturals

procedure p48107 is
   a: Natural;
   b: Positive;
begin
   Ada.Integer_Text_IO.Get(a);
   Ada.Integer_Text_IO.Get(b);

   Ada.Integer_Text_IO.Put(Item => a / b, Width => 0);
   Ada.Text_IO.Put(" ");
   Ada.Integer_Text_IO.Put(Item => a mod b, Width => 0);
   Ada.Text_IO.New_Line;
end p48107;
