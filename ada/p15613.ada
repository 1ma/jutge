with Ada.Integer_Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Temperatures

procedure p15613 is
   subtype Temperatura is Integer range -273 .. Integer'Last;

   t: Temperatura;

   Punt_Congelacio: constant Temperatura := 0;
   Punt_Fredor:     constant Temperatura := 10;
   Punt_Calor:      constant Temperatura := 30;
   Punt_Ebullicio:  constant Temperatura := 100;
begin
   Ada.Integer_Text_IO.Get(t);

   if t < Punt_Fredor then
      Ada.Text_IO.Put_Line("fa fred");
   elsif t > Punt_Calor then
      Ada.Text_IO.Put_Line("fa calor");
   else
      Ada.Text_IO.Put_Line("s'esta be");
   end if;

   if t <= Punt_Congelacio then
      Ada.Text_IO.Put_Line("l'aigua es gelaria");
   end if;

   if t >= Punt_Ebullicio then
      Ada.Text_IO.Put_Line("l'aigua bulliria");
   end if;
end p15613;
