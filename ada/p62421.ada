with Ada.Strings.Unbounded;
with Ada.Strings.Unbounded.Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Tres paraules

procedure p62421 is
   use Ada.Strings.Unbounded;
   use Ada.Strings.Unbounded.Text_IO;

   function Read_Word return Unbounded_String is
      us: Unbounded_String;
      ch: Character;
   begin
      loop
         Ada.Text_IO.Get(Item => ch);
         exit when ch = ' ';
         Append(us, ch);
         exit when Ada.Text_IO.End_Of_Line;
      end loop;

      return us;
   end Read_Word;

   a: Unbounded_String := Read_Word;
   b: Unbounded_String := Read_Word;
   c: Unbounded_String := Read_Word;
begin
   Put_Line(c & ' ' & b & ' ' & a);
end p62421;
