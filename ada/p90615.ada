with Ada.Integer_Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Màxim de tres enters

procedure p90615 is
   i, j, k: Integer;
begin
   Ada.Integer_Text_IO.Get(i);
   Ada.Integer_Text_IO.Get(j);
   Ada.Integer_Text_IO.Get(k);

   Ada.Integer_Text_IO.Put(Item => Integer'Max(Integer'Max(i, j), k), Width => 0);
end p90615;
