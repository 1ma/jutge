with Ada.Integer_Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Màxim de dos enters

procedure p56118 is
   i, j: Integer;
begin
   Ada.Integer_Text_IO.Get(i);
   Ada.Integer_Text_IO.Get(j);

   Ada.Integer_Text_IO.Put(Item => Integer'Max(i, j), Width => 0);
end p56118;
