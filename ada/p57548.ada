with Ada.Integer_Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Suma de dos enters

procedure p57548 is
   i, j: Integer;
begin
   Ada.Integer_Text_IO.Get(i);
   Ada.Integer_Text_IO.Get(j);

   -- Sense Width => 0 Put deixa un espai blanc per al
   -- '-' encara que el nombre no sigui negatiu...
   Ada.Integer_Text_IO.Put(Item => i + j, Width => 0);
end p57548;
