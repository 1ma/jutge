with Ada.Text_IO;
with Ada.Integer_Text_IO;

-- Learning to program (Introduction)
-- Suma d'un segon

procedure p34279 is
   type Hora is mod 24;
   type Minut is mod 60;
   type Segon is mod 60;

   type Rellotge is record
      h: Hora;
      m: Minut;
      s: Segon;
   end record;

   procedure Afegir_Segon(r: in out Rellotge) is
   begin
      r.s := r.s + 1;

      if r.s = 0 then
         r.m := r.m + 1;

         if r.m = 0 then
            r.h := r.h + 1;
         end if;
      end if;
   end Afegir_Segon;

   procedure Printar_Rellotge(r: in Rellotge) is
   begin
      if r.h < 10 then
         Ada.Text_IO.Put("0");
      end if;
      Ada.Integer_Text_IO.Put(Item => Integer(r.h), Width => 0);

      Ada.Text_IO.Put(":");

      if r.m < 10 then
         Ada.Text_IO.Put("0");
      end if;
      Ada.Integer_Text_IO.Put(Item => Integer(r.m), Width => 0);

      Ada.Text_IO.Put(":");

      if r.s < 10 then
         Ada.Text_IO.Put("0");
      end if;
      Ada.Integer_Text_IO.Put(Item => Integer(r.s), Width => 0);

      Ada.Text_IO.New_Line;
   end Printar_Rellotge;

   r: Rellotge;
   h, m, s: Integer;
begin
   Ada.Integer_Text_IO.Get(h);
   Ada.Integer_Text_IO.Get(m);
   Ada.Integer_Text_IO.Get(s);

   r := (Hora(h), Minut(m), Segon(s));

   Afegir_Segon(r);
   Printar_Rellotge(r);
end p34279;
