with Ada.Integer_Text_IO;
with Ada.Text_IO;

-- Learning to program (Introduction)
-- Divisió entera i residu d'un enter amb un natural

procedure p92351 is
   a, d: Integer;
   b: Positive;
   r: Natural;
begin
   Ada.Integer_Text_IO.Get(a);
   Ada.Integer_Text_IO.Get(b);

   d := a / b;
   r := a mod b;

   if a < 0 and r /= 0 then
      d := d - 1;
   end if;

   Ada.Integer_Text_IO.Put(Item => d, Width => 0);
   Ada.Text_IO.Put(" ");
   Ada.Integer_Text_IO.Put(Item => r, Width => 0);
   Ada.Text_IO.New_Line;
end p92351;
